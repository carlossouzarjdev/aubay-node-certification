const Hapi = require("hapi")
const server = new Hapi.Server()

server.connection({ port: 3000 })

server.route([{
    method: ["GET", "POST"],
    path: "/",
    handler: function (req, resp) {
        return resp("hello World " + req.route.method)

    }
}, {
    method: "GET",
    path: "/hello",
    handler: function (req, resp) {
        return resp("hello World 2")
    }
}, {
    method: "GET",
    path: "/nature/flowers/orchids/{name}.{ext}",
    handler: function (req, resp) {
        const name = req.params.name
        const ext = req.params.ext

        resp("request " + name + " ext " + ext)
    }

}
    , {
    method: "GET",
    path: "/cat/{subcat1}/{subcat2}/{subcat3}/{name}.{ext}",
    handler: function (req, resp) {
        const name = req.params.name
        const ext = req.params.ext
        const subcat2 = req.params.subcat2

        resp("request " + name + " ext " + ext + " subcat2 " + subcat2)
    }

}, {
    method: "GET",
    path: "/cat2/{subcat*3}/{name}.{ext}",
    handler: function (req, resp) {
        const name = req.params.name
        const ext = req.params.ext
        const subcat = req.params.subcat

        resp("request " + name + " ext " + ext + " subcat " + subcat)
    }

}, {
    method: "GET",
    path: "/team/{name?}",
    handler: function (req, resp) {
        if (req.params.name)
            return resp(req.params.name)

        else
            return resp("olaaaa")
    }

}, {
    method: "GET",
    path: "/{path*}",
    handler: function (req, resp) {
        resp("End of my site....")
    }

}
])

server.start((err) => {

    if (err) {
        throw err
    }

    console.log("running at:" + server.info.uri)
})

