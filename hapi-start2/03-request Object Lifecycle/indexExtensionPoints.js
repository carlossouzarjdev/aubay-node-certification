const Hapi = require("hapi")
const Netmask = require("netmask").Netmask
const server = new Hapi.Server()
const blacklist = ["192.168.1.1","127.0.0.1"]

server.connection({port:3000})

server.ext("onRequest", (req,resp) => {

    const ip = req.info.remoteAddress

    for(var i=0;  i<blacklist.length; i++){
        const block = new Netmask(blacklist[i])

        if(block.contains(ip)){
            console.log("ip blocked")
            return resp("blocked")
        }
    }

    resp.continue();

})

server.route({
    method: "GET",
    path: "/hello",
    handler: function(req,resp){
        return resp("hello World")
    }

})

server.start((err) => {

    if(err) {
        throw err
    }

console.log("running at:"+ server.info.uri)
})

