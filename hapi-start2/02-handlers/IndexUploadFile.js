const Hapi = require("hapi")
const Inert = require("inert")
const Path = require("path")
const Fs = require("fs")

const server = new Hapi.Server()

server.connection({port:3000})

server.route({
    method: "GET",
    path: "/hello",
    handler: function(req,resp){
        
        resp("hello")

    }
})

server.register(Inert, (err) => {

    if(err) throw err

    server.route({
        method: "POST",
        path: "/upload", 
        handler: function(req,resp){
            const targetPath = Path.join(__dirname,"uploads",Path.basename(req.payload.upload.filename))
            const tempPath = req.payload.upload.path

            Fs.rename(tempPath, targetPath, (err) => {
                if (err) throw err

            })
            resp("ok")
        },
        config: {
            payload: {
                parse: true,
                output: "file"
            }  
        }  
})

    server.route({
        method: "GET",
        path: "/assets/{path*}", 
        handler: {
            directory: {
                path: Path.join(__dirname,"assets")
            }
        }      
    })

    server.start((err) => {
        if(err) throw err

        console.log("running at:"+ server.info.uri)
    })
})


