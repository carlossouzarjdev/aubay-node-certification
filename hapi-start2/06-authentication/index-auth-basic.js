const Hapi = require("hapi")
const AuthBasic = require("hapi-auth-basic")
const validUsers = require("./valid-users")
const server = new Hapi.Server()

server.connection({ port: 3000 })


server.register(AuthBasic, (err) => {
    if (err) throw err

    server.route({
        method: "GET",
        path: "/",
        handler: function (req, resp) {
            return resp("hello World")
        }

    })

    server.start((err) => {

            if (err) {
                throw err
            }

            console.log("running at:" + server.info.uri)
    })

})

