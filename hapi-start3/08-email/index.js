const Hapi = require("hapi")
const nodemailer = require("nodemailer")

const server = new Hapi.Server()

server.connection({port:3000})

server.route({
    method: "GET",
    path: "/hello",
    handler: function(req,resp){
        return resp("hello World")
    }

})

server.route({
    method: "GET",
    path: "/email",
    handler: function(req,resp){
        const transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                user: "teste32a@gmail.com",
                pass: "Pa$$w0rd$"
            }
        })

        let mailOptions = {
            from: "teste32a@gmail.com",
            to: "rjam.pinto@gmail.com",
            subject: "teste",
            text: "Hi!"
        }

        transporter.sendMail(mailOptions,function(error,info){
            if (error)
                console.log(error)
            
            else
            resp("ok")
            
        })
    }

})

server.start((err) => {

    if(err) {
        throw err
    }

console.log("running at:"+ server.info.uri)
})

