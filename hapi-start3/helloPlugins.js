const Hapi = require("hapi")
const Good = require("good")

const server = new Hapi.Server()

server.connection({ port: 3000 })

server.route({
    method: "GET",
    path: "/",
    handler: function (req, resp) {
        return resp("hello World")
    }

})

server.register([{
     register: require("./plugin-route")
},
{
    register: Good,
    options: {
        reporters: {
            console: [{
                module: "good-squeeze",
                name: "Squeeze",
                args: [{
                    response: "*",
                    log: "*"
                }]
            },
            {
                module: "good-console"
            }, "stdout"]
        }
    }
}], (err) => {
    if (err) {
        throw err
    }
    server.start((err) => {

        if (err) {
            throw err
        }

        console.log("running at:" + server.info.uri)
    })

})

