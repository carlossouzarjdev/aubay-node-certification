const Hapi = require("hapi")
const AuthBasic = require("hapi-auth-basic")
const validUsers = require("./valid-users")
const server = new Hapi.Server()

server.connection({ port: 3000 })

const validate = function(req,username,password,callback){
    const err = null
    let isValid = false
    let credentials={}
    if(validUsers.validateUsers(username,password)){
        isValid = true
        credentials = {username:username, scope:validUsers.getScope(username)}
        callback(err,isValid,credentials)
    }
}

server.register(AuthBasic, (err) => {
    if (err) throw err

    server.auth.strategy("simple","basic",{validateFunc: validate})

    server.route({
        method: "GET",
        path: "/",
        config: {
            auth:{strategy: "simple", scope: "admin"},
            handler: function (req, resp) {
                resp("hello World")
            }
        }
    })

    server.start((err) => {

            if (err) {
                throw err
            }

            console.log("running at:" + server.info.uri)
    })

})

