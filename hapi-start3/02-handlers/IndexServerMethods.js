const Hapi = require("hapi")
const server = new Hapi.Server()

server.connection({port:3000})

const mean = function(values,next){
    const sum = values.reduce((a,b) => a+b)
    return next(null, sum/values.length)
}

server.method("mean",mean,{})

server.route({
    method: "POST",
    path: "/hello",
    handler: function(req,resp){
        
        server.methods.mean(req.payload.values,(err,result) => {
            if(err) throw err

            resp({mean: result})

        })
    }
})

server.start((err) => {

    if(err) throw err

console.log("running at:"+ server.info.uri)
})

