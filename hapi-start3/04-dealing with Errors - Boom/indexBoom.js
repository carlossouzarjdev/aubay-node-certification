const Hapi = require("hapi")
const Netmask = require("netmask").Netmask
const Boom = require("boom")
const Vision = require("vision")
const Path = require("path")
const Handlerbars = require("handlebars")


const server = new Hapi.Server()
const blacklist = ["192.168.1.1","127.0.0.2"]

server.connection({port:3000})

server.ext("onRequest", (req,resp) => {

    const ip = req.info.remoteAddress

    for(var i=0;  i<blacklist.length; i++){
        const block = new Netmask(blacklist[i])

        if(block.contains(ip)){
            console.log("ip blocked")
            return resp(Boom.forbidden("blocked"))
        }
    }

    resp.continue();

})

server.route({
    method: "GET",
    path: "/error",
    handler: function(req,resp){
        return resp("Internal Server Error").code(500)
    }

})


server.route({
    method: "GET",
    path: "/js-error",
    handler: function(req,resp){
        resp(new Error("Something bad happened!"))
    }

})


server.route({
    method: "GET",
    path: "/newfeature",
    handler: function(req,resp){
        resp(Boom.notImplemented())
    }

})


server.ext("onPreResponse",(req,resp) => {

    if(req.response.isBoom){
        const status = req.response.output.payload.statusCode
        const errName = req.response.output.payload.error
        const errMessage = req.response.output.payload.message

        return resp.view("error", {
            status: status,
            errName: errName,
            errMessage: errMessage
        }).code(parseInt(status))

    }

    repp.continue()

})

server.register(Vision , () => {

    server.views({
        engines: {
            hbs: Handlerbars
        },
        path: Path.join(__dirname, "templates")
    })

    server.start((err) => {

        if(err) throw err

        console.log("running at:"+ server.info.uri)
    })
})



