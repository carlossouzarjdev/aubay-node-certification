const Hapi = require("hapi")
const server = new Hapi.Server()

server.connection({port:3000})

server.route({
    method: "GET",
    path: "/hello",
    handler: function(req,resp){

        resp("hello word").type("text/plain").code(200).header("x-powered-by", "aubay")

    }

})

server.ext("onPreResponse",(req,res) => {

    const response = req.response
    console.log(response.statusCode)
    console.log(response.headers)
    res.continue()

})


server.start((err) => {

    if(err) {
        throw err
    }

console.log("running at:"+ server.info.uri)
})

