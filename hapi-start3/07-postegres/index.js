const Hapi = require("hapi")
const server = new Hapi.Server()
const pupies = require("./pgQueries")
const swagger = require("hapi-swagger")

server.connection({port:3000})

server.route({
    method: "GET",
    path: "/hello",
    handler: function(req,resp){
        return resp("hello World")
    }

})

server.route({
    method: "GET",
    path: "/api/pupies",
    handler: function(req,resp){
        pupies.getAllPuppies(req,resp,(err) => {

        console.log(err.name)
        console.log("...........")
        console.log(err)
        resp(err)
        })

    },
    config:{
        description: "get pupies",
        notes: "all kind",
        tags: ["api"]
    }

})

server.route({
    method: "POST",
    path: "/api/pupies",
    handler: function(req,resp){
        pupies.createPuppy(req,resp,(err) => {

        console.log(err.name)
        console.log("...........")
        console.log(err)
        resp(err)

        })

    },
    config:{
        description: "insert pupies",
        notes: "all kind",
        tags: ["api"]
    }

})

server.route({
    method: "PUT",
    path: "/api/pupies/{id}",
    handler: function(req,resp){
        pupies.updatePuppy(req,resp,(err) => {

        console.log(err.name)
        console.log("...........")
        console.log(err)
        resp(err)

        })

    },
    config:{
        description: "update pupies",
        notes: "all kind",
        tags: ["api"]
    }

})
server.route({
    method: "DELETE",
    path: "/api/pupies/{id}",
    handler: function(req,resp){
        pupies.deletePuppy(req,resp,(err) => {

        console.log(err.name)
        console.log("...........")
        console.log(err)
        resp(err)

        })

    },
    config:{
        description: "delete pupies",
        notes: "all kind",
        tags: ["api"]
    }

})
server.route({
    method: "GET",
    path: "/api/pupies/{id}",
    handler: function(req,resp){
        pupies.getPuppy(req,resp,(err) => { 

        console.log(err.name)
        console.log("...........")
        console.log(err)
        resp(err)

        })
    },
    config:{
        description: "get pupies",
        notes: "all kind",
        tags: ["api"]
    }

})



server.register([{
    register: swagger,
    options: {
        info: {
            "title":"Api documentation", "version": "1.0.0"
        }
    }
}, {register: require("inert")
}, {register: require("vision")
}], (err) => {
    if(err) {
        throw err
    }
    server.start((err) => {

        if(err) {
            throw err
        }

    console.log("running at:"+ server.info.uri)
    })
})

