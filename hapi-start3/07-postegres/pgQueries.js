const promise = require("blueBird")

var options = {
    promiseLib: promise

} 

var pgp = require("pg-promise")(options)

var cnString = "postgres://postgres:123456@localhost:5432/puppies"
var db = pgp(cnString)

function getAllPuppies(req,resp,next){
    db.any("SELECT * FROM pups")
    .then( (dados) => {
        resp(dados).code(200)

    })
    .catch((err) => {
        return next(err)
    })
}

function getPuppy(req,resp,next){
    var id = req.params.id
    db.one("SELECT * FROM pups WHERE id = $1", id)
    .then( (dados) => {
        resp(dados).code(200)

    })
    .catch((err) => {
        return next(err)
    })
}


function createPuppy(req,resp,next){
    req.payload.age = parseInt(req.payload.age)
    db.none("INSERT INTO pups (name, bread, age, sex) VALUES(${name},${bread},${age},${sex})", req.payload)
    .then( (dados) => {
        resp(dados).code(200)

    })
    .catch((err) => {
        return next(err)
    })
}

function updatePuppy(req,resp,next){
    req.payload.age = parseInt(req.payload.age)
    req.payload.id = parseInt(req.payload.id)

    db.query("UPDATE pups SET name=${name}, bread=${bread}, age=${age}, sex=${sex} WHERE id = ${id}", req.payload)
    .then( (dados) => {
        resp(dados).code(200)

    })
    .catch((err) => {
        return next(err)
    })
}

function deletePuppy(req,resp,next){
    var id = req.params.id

    db.query("DELETE FROM pups WHERE id = $1)", id)
    .then( (dados) => {
        resp(dados).code(200)

    })
    .catch((err) => {
        return next(err)
    })
}

module.exports = {
    getAllPuppies: getAllPuppies,
    getPuppy: getPuppy,
    createPuppy: createPuppy,
    updatePuppy: updatePuppy, 
    deletePuppy: deletePuppy 
    
    
}