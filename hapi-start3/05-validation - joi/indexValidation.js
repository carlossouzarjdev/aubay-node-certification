const Hapi = require("hapi")
const Joi = require("joi")
const server = new Hapi.Server()

server.connection({port:3000})

var aux="12"
Joi.validate(aux,Joi.number().min(50),(err) => {

    if(err) 
        console.log("invalid")
    else
    console.log("valid")

})

server.route({
    method: "GET",
    path: "/data/{id}/{name?}",
    handler: function(req,resp){
        return resp(req.params.id)
    },
    config: {
        validate: {
            params: {
                id: Joi.number(),
                name: Joi.string().min(2)
            }
        }
    }

})

server.route({
    method: "POST",
    path: "/data",
    handler: function(req,resp){
        resp(req.payload)

    },
    config: {
        validate: {
            payload: {
                id: Joi.number(),
                name: Joi.string().min(2),
                datetime: Joi.date(),
                country: Joi.string().valid(["pt","br","es","fr","de"])
            },
            failAction : function(req,resp,source,error){
                resp("has aninvalid input").code(400)
            }
        }
    }

})

server.start((err) => {

    if(err) {
        throw err
    }

console.log("running at:"+ server.info.uri)
})

