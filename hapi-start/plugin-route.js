var pluginRoute = {
    register: function (server, options, next) {
        server.route({
            method: "GET",
            path: "/{name}",
            handler: function (req, resp) {
                resp("Hello " + encodeURIComponent(req.params.name))
            }

        })
        next()
    }
}

pluginRoute.register.attributes = {
    name: "plugin-routes",
    version: "1.0.0"
}

module.exports = pluginRoute