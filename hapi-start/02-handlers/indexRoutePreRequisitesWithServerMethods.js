const Hapi = require("hapi")
const Fs = require("fs")

const server = new Hapi.Server()

server.connection({ port: 3000 })

server.method("text", (req, resp) => {
    Fs.readFile("./text.txt", (err, data) => {

        if (err)
            throw err

        resp(data.toString())
    })
})

server.route({
    method: "GET",
    path: "/hello",
    handler: function (req, resp) {
        return resp(req.pre.text)
    },
    config: {
        pre: ["text"]
    }

})

server.start((err) => {

    if (err) {
        throw err
    }

    console.log("running at:" + server.info.uri)
})

