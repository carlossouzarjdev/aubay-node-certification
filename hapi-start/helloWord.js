const Hapi = require("hapi")
const server = new Hapi.Server()

server.connection({port:3000})

server.route({
    method: "GET",
    path: "/hello",
    handler: function(req,resp){
        return resp("hello World")
    }

})

server.start((err) => {

    if(err) {
        throw err
    }

console.log("running at:"+ server.info.uri)
})

